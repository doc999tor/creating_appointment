exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://0.0.0.0:4444/',
    allScriptsTimeout: 3000,
    chromeOnly: true,
    directConnect: true,
    specs: ['client/app/**/*.prot.js']
};
