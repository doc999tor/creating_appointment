import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Components from './components/components';
import Pages from './pages/pages';
import services from './services/services';
import AppComponent from './app.component';
import angularLocalStorage from 'angular-local-storage';
import 'angular-date-picker';
import 'angular-time-picker';
import 'normalize.css';


angular.module('app', [
    uiRouter,
    Components,
    Pages,
    services,
    angularLocalStorage,
    'mp.datePicker',
    'wingify.timePicker'
  ])
  .constant("config", window.config)
  .config(($locationProvider) => {
    "ngInject";
    $locationProvider.html5Mode(true).hashPrefix('!');
  })
  .run((config, $rootScope, UrlConfiguration) => {
    "ngInject";
    $rootScope.APP = config;

  })

.component('app', AppComponent);
