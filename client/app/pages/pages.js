import angular from 'angular';
import Home from './home/home';
import Procedure from './procedure/procedure';
import Swiper from './swiper/swiper';
import proceduresSummary from './procedures-summary/procedures-summary';


let pagesModule = angular.module('app.pages', [
    Home,
    Procedure,
    Swiper,
    proceduresSummary
  ])

  .name;

export default pagesModule;
