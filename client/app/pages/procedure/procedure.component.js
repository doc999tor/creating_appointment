import template from './procedure.html';
import controller from './procedure.controller';
import './procedure.scss';

let procedureComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default procedureComponent;
