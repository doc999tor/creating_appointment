import angular from 'angular';
import uiRouter from 'angular-ui-router';
import procedureComponent from './procedure.component';

let procedureModule = angular.module('procedure', [
    uiRouter
  ])



  .component('procedure', procedureComponent)

  .name;

export default procedureModule;
