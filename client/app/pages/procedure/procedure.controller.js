class ProcedureController {
  constructor(HttpRouter, UrlConfiguration, $rootScope) {
    "ngInject";
    this.$rootScope = $rootScope;
    this.UrlConfiguration = UrlConfiguration;
    this.HttpRouter = HttpRouter;
    this.procedureList = {};
    this.selectedList = [];
    this.procedureTime = "";
  }
  callOnLoad() {
    this.procedureList = this.$rootScope.APP.procedureList;
    this.$rootScope.$on('clearProcedureList', () => {
      this.selectedList = [];
    });
    this.$rootScope.$on('swipe:trainMove', (event, data) => {
      if (data === "procedure") {
        this.UrlConfiguration.setUrl(['date', 'start', 'end', 'client_id', 'procedure_id']);
      }
    });
    this.$rootScope.$on('search:change', (event, data) => {
      if (data.nameOfPage === "procedure") {
        this.searcheableName = data.text;
        this.HttpRouter.getBackEndData('procedures', data.text, 0).then((responce) => {
          this.procedureList.bi = responce.bi;
          this.procedureList.all = responce.all;
        });
      }
    });
    this.$rootScope.$on('summaryProcedure:changeTime', (event, data) => {
      this.addNewTimeCloud(data.time, data.sign);
    });
  }
  selectProcedure(id, price, time, name) {
    this.selectedList.push({
      id: id,
      price: price,
      time: time,
      name: name
    });
    this.addTime(time, id);
    this.$rootScope.$emit('selectedListUpdate', this.selectedList);
  }
  addTime(additableMinutes, id) {
    this.addNewTimeCloud(additableMinutes, 1);
    this.UrlConfiguration.addIdForProcedure(id);
  }
  removeProcedure(id, additableMinutes) {
    for (let i = 0; i < this.selectedList.length; i++) {
      if (this.selectedList[i].id === id) {
        this.selectedList.splice(i, 1);
        break;
      }
    }
    this.$rootScope.$emit('selectedListUpdate', this.selectedList);
    this.addNewTimeCloud(additableMinutes, -1);
    this.UrlConfiguration.deleteProcedure(id);
  }
  addNewTimeCloud(additableMinutes, sign) {
    let curentUrlData = this.UrlConfiguration.getUrlDataObject(),
      property, oldTime, oldDate, lastString;
    property = curentUrlData.procedure_time ? 'procedure_time' : 'start';
    const fixTime = time => {
      if (time.toString().length === 1) {
        time = "0" + time;
      }
      return time;
    }
    oldTime = {
      hours: curentUrlData[property].split(':')[0],
      minutes: curentUrlData[property].split(':')[1]
    }
    oldDate = new Date(99, 5, 24, oldTime.hours, oldTime.minutes);
    oldDate.setMinutes(oldDate.getMinutes() + additableMinutes * sign);
    lastString = fixTime(oldDate.getHours()) + ":" + fixTime(oldDate.getMinutes());
    this.UrlConfiguration.changeUrl(lastString, 'procedure_time', false);
    if (!curentUrlData.isFixedEndTime) {
      this.UrlConfiguration.changeUrl(lastString, "end", true);
      this.$rootScope.$emit('dataPicker', {
        prop: 'end',
        value: lastString
      });
    }

    if (curentUrlData.end) {
      this.checkOnCloudLimit();
    }
  }
  checkOnCloudLimit() {
    let curentUrlData = this.UrlConfiguration.getUrlDataObject(),
      time = {
        end: {
          minutes: curentUrlData.end.split(":")[1],
          hours: curentUrlData.end.split(":")[0]
        },
        cloud: {
          minutes: curentUrlData.procedure_time.split(":")[1],
          hours: curentUrlData.procedure_time.split(":")[0]
        }
      }
      // if (+time.cloud.hours > +time.end.hours || (+time.cloud.minutes > +time.end.minutes && +time.cloud.hours === +time.end.hours)) {
      //   alert("You have exceeded the limit of your time");
      // }
  }

}
export default ProcedureController;
