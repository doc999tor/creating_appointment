var constFunctions = require('./../../../protractorConstance');
describe("Procedure works", function() {
  it('Select procedure', () => {
    constFunctions.loadSwipe();
    var procedureList = element.all(by.css('.add-procedure-button'));
    procedureList.count()
      .then(function(count) {
        let countOfRandomElements = Math.floor((Math.random() * 8)+1),
          i, array = [];
        for (i = 0; i < countOfRandomElements; i++) {
          array.push(Math.floor((Math.random() * count)));
        }
        for (i = 0; i < countOfRandomElements; i++) {
          procedureList.get(array[i]).click();
          browser.sleep(500);
        };
        var cancelProcedureButton = element.all(by.css('.cancel-procedure-button'));
        cancelProcedureButton.count()
          .then(function(count) {
            for (i = countOfRandomElements - 1; i >= 0; i--) {
              cancelProcedureButton.get(i).click();
              browser.sleep(500);
            };
          });
      });
  });
});
