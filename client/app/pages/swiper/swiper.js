import angular from 'angular';
import uiRouter from 'angular-ui-router';
import swiperComponent from './swiper.component';

let swiperModule = angular.module('swiper', [
    uiRouter
  ])

  .config(($stateProvider, $urlRouterProvider) => {
    "ngInject";

    $urlRouterProvider.otherwise('/creating-appointment');

    $stateProvider
      .state('swiper', {
        url: '/creating-appointment',
        component: 'swiper'
      });
  })

  .component('swiper', swiperComponent)

  .name;

export default swiperModule;
