class SwiperController {

  constructor($window, $location, $compile, $element, $scope, $timeout, $rootScope, UrlConfiguration) {
    "ngInject";
    this.$rootScope = $rootScope;
    this.$scope = $scope;
    this.$timeout = $timeout;
    this.$compile = $compile;
    this.$element = $element;
    this.$window = $window;
    this.$location = $location;
    this.UrlConfiguration = UrlConfiguration;
    this.styleTrain = {
      left: 0
    };
    this.curentWidthTrain = 0;
    this.countOfPages = $rootScope.APP.listOfPages.length;
    this.curentSwipePosition = 1;
    this.listOfPages = $rootScope.APP.listOfPages;
  }
  changePosition(number) {
    this.curentWidthTrain += 100 * number;
    this.curentSwipePosition += -1 * number;
    this.styleTrain = {
      left: this.curentWidthTrain + 'vw'
    };
    this.$rootScope.curentSwipeName = this.listOfPages[this.curentSwipePosition - 1].name;
    this.$rootScope.curentSwipePosition = this.curentSwipePosition;
    this.$rootScope.$emit('swipe:trainMove', this.$rootScope.curentSwipeName);
  }
  swipeLeft() {
    if (this.curentSwipePosition !== this.countOfPages) {
      this.changePosition(-1);
      if (this.curentSwipePosition !== this.countOfPages) {
        this.createPage(this.curentSwipePosition, 0);
      }
    }
    if (this.curentSwipePosition >= 3) {
      this.deletePage(3);
    }

  }
  swipeRihgt() {
    if (this.curentSwipePosition !== 1) {
      this.changePosition(1);
      if (this.curentSwipePosition !== 1) {
        this.createPage(this.curentSwipePosition - 2, 2);
      }
    }
    if (this.curentSwipePosition <= this.countOfPages - 2) {
      this.deletePage(-1);
    }
  }
  createPage(number, shift) {
    let text = '<div class="swipe-carriage">' + this.listOfPages[number].component + '</div>';
    let template = angular.element(this.$compile(text)(this.$scope));
    this.$element[0].querySelector('.swipe-train').children[this.curentSwipePosition - shift].replaceWith(template[0]);
    this.$rootScope.$emit("replace:element", this.listOfPages[number].name)
  }
  deletePage(shift) {
    this.$timeout(() => {
      let text = '<div class="swipe-carriage"></div>',
        template = angular.element(this.$compile(text)(this.$scope));
      this.$element[0].querySelector('.swipe-train').children[this.curentSwipePosition - shift].replaceWith(template[0]);
    }, 500)
  }
  parseUrl() {
    let urlData = {
      start: "00:00",
      end: "",
      date: "2017-01-01",
      procedure_time: "",
      procedure_id: [],
      client_id: "",
      comments: "",
      isFixedEndTime: false
    };
    let searchParam = this.$location.$$search
    for (let i in searchParam) {
      for (let j in urlData) {
        if (i === j) {
          urlData[j] = searchParam[i];
        }
      }
    }
    if (urlData.procedure_id.length > 0) {
      let stringToArray = urlData.procedure_id.split(',');
      urlData.procedure_id = [];
      for (let z in stringToArray) {
        urlData.procedure_id.push(stringToArray[z]);
      }
    }
    urlData.isFixedEndTime = (!!urlData.end);
    this.UrlConfiguration.setUrlData(urlData);
  }
  onLoadPage() {
    this.parseUrl();
    this.$timeout(() => {
      this.createPage(0, 1);
      this.createPage(1, 0);
      this.$rootScope.$on('swiper:changePosition', (event, data) => {
        if (data === 'left') {
          this.swipeRihgt();
        } else {
          this.swipeLeft();
        }
      });
    }, 0)

  }

}
export default SwiperController;
