import template from './swiper.html';
import controller from './swiper.controller';
import './swiper.scss';

let swiperComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default swiperComponent;
