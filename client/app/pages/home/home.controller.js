class HomeController {

  constructor($state, HttpRouter, $scope, $window, $rootScope, UrlConfiguration) {
    "ngInject";
    this.UrlConfiguration = UrlConfiguration;
    this.$rootScope = $rootScope;
    this.$scope = $scope;
    this.$window = $window;
    this.HttpRouter = HttpRouter;
    this.$state = $state;
    this.home = {};
    this.searcheableName = '';
    this.urlMessage = 'avatar/';
    this.isEndUserList = false;
    this.countOfUser = 12;
    this.isShowSpiner = false;
    this.warperHome = "";
  }
  callOnLoad() {
    this.$rootScope.$on('replace:element', (event, data) => {
      if (data === 'home') {
        this.startLoadScripts();
      }
    });
    this.$rootScope.$on('swipe:trainMove', (event, data) => {
      if (data === "home") {
        this.UrlConfiguration.setUrl(['date', 'start', 'end']);
      }
    });
  }
  startLoadScripts() {
    this.all = this.$rootScope.APP.users.all;
    this.bi = this.$rootScope.APP.users.bi;
    this.$rootScope.$on('search:change', (event, data) => {
      if (data.nameOfPage === "home") {
        this.searcheableName = data.text;
        this.isShowSpiner = true;
        this.countOfUser = 12;
        this.HttpRouter.getBackEndData('clients',data.text, 0).then((responce) => {
          this.bi = responce.bi;
          this.all = responce.all;
          this.isShowSpiner = false;
        });
      }
    });
    this.warperHome = document.getElementById('home-wraper');
    this.warperHome.onscroll = () => {
      let B = document.body,
        DE = document.documentElement,
        O = Math.min(B.clientHeight, DE.clientHeight),
        S = this.warperHome.scrollHeight - this.warperHome.scrollTop
      if (O === S && !this.isEndUserList) {
        this.isShowSpiner = true;
        this.countOfUser += 12;
        this.HttpRouter.getBackEndData('clients',this.searcheableName, this.countOfUser).then((data) => {
          if (data) {
            this.all = this.all.concat(data.all);
          } else {
            this.isEndUserList = true;
          }
          this.isShowSpiner = false;
        });
      }
    }
  }

}
export default HomeController;
