'use strict';
describe('Start test', function() {
  it('The URL read correctly', function() {
    browser.get('http://localhost:3000');
    var html_time = element(by.id('url-time')),
      html_date = element(by.id('url-date'));
    browser.ignoreSynchronization = true
    browser.getCurrentUrl().then(function(url) {
      var url_data = url.split("?")[1],
        url_time = url_data.split("&")[0].split('=')[1],
        url_date = url_data.split("&")[1].split('=')[1];
      html_time.getText().then(function(text) {
        expect(url_time === text.split('-')[0]);
      });
      html_date.getText().then(function(text) {
        expect(url_date === text);
      });
    });
  });
});
