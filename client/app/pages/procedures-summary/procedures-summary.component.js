import template from './procedures-summary.html';
import controller from './procedures-summary.controller';
import './procedures-summary.scss';

let proceduresSummaryComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default proceduresSummaryComponent;
