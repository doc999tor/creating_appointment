import LocalStorageModule from 'angular-local-storage';

class ProceduresSummaryController {
  constructor(UserConfiguration, UrlConfiguration, $rootScope) {
    "ngInject";
    this.$rootScope = $rootScope;
    this.UrlConfiguration = UrlConfiguration;
    this.UserConfiguration = UserConfiguration;
    this.currentUser = {};
    this.selectedList = [];
    this.finallyPrice = 0;
    this.finallyTime = 0;
    this.summaryTime = {
      hours: 0,
      minutes: 0
    }
  }
  callOnLoad() {
    this.$rootScope.$on('selectedListUpdate', (event, data) => {
      this.selectedList = data;
      this.calculatePrice();
    });
    this.$rootScope.$on('swipe:trainMove', (event, data) => {
      if (data === "procedure") {
        this.UrlConfiguration.setUrl(['date', 'start', 'end', 'client_id', 'procedure_id']);
      }
    });
    this.$rootScope.$on('user:update', () => {
      this.currentUser = this.UserConfiguration.get();
    });
  }
  removeProcedure(id, additableMinutes) {
    for (let i = 0; i < this.selectedList.length; i++) {
      if (this.selectedList[i].id === id) {
        this.selectedList.splice(i, 1);
        break;
      }
    }
    this.$rootScope.$emit('summaryProcedure:changeTime', {time:additableMinutes,sign:-1});
    this.UrlConfiguration.deleteProcedure(id);
    this.calculatePrice();
  }
  calculatePrice() {
    this.finallyPrice = 0;
    this.finallyTime = 0;
    angular.forEach(this.selectedList, (value) => {
      this.finallyPrice += value.price;
      this.finallyTime += value.time;
    });
    this.setFinallyTime(0, 0, this.finallyTime, 1);
  }
  setFinallyTime(hours, minutes, countMinutes, sign) {
    let newTime = new Date(99, 5, 24, hours, minutes);
    newTime.setMinutes(newTime.getMinutes() + countMinutes * sign);
    this.summaryTime.hours = newTime.getHours();
    this.summaryTime.minutes = newTime.getMinutes();
  }
  changeTime(sign) {
    this.setFinallyTime(this.summaryTime.hours, this.summaryTime.minutes, this.$rootScope.APP.stepForChangeFinallyTime, sign);
    this.$rootScope.$emit('summaryProcedure:changeTime', {time: this.$rootScope.APP.stepForChangeFinallyTime,sign:sign});
  }

}
export default ProceduresSummaryController;
