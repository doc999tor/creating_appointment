import angular from 'angular';
import uiRouter from 'angular-ui-router';
import proceduresSummaryComponent from './procedures-summary.component';

let proceduresSummaryModule = angular.module('proceduresSummary', [
    uiRouter
  ])

  .config(($stateProvider, $urlRouterProvider) => {
    "ngInject";

    $stateProvider
      .state('proceduresSummary', {
        url: '/proceduresSummary',
        component: 'proceduresSummary',
        params: {
            obj: null
        }
      });
  })

  .component('proceduresSummary', proceduresSummaryComponent)

  .name;

export default proceduresSummaryModule;
