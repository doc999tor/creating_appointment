export default function() {

  return function(scope, iElement, iAttrs) {
    iElement.bind('error', function() {
      angular.element(this).attr("src", iAttrs.fallbackSrc);
    });
  }
}
