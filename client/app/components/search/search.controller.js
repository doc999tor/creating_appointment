class searchController {
  constructor($rootScope) {
    "ngInject";
    this.$rootScope = $rootScope;
  }
  makeSearch() {
    var data = {
      nameOfPage: this.$rootScope.curentSwipeName,
      text: this.searcheableName
    };
    this.$rootScope.$emit('search:change', data);
  }
  forward() {
    this.$rootScope.$emit('swiper:changePosition', 'right');
  }
}

export default searchController;
