import angular from 'angular';
import Datepicker from './datepicker/datepicker';
import UserList from './userList/userList';
import search from './search/search';
import Header from './header/header';
import commentArea from './commentArea/commentArea';


let componentsModule = angular.module('app.components', [
    Datepicker, UserList, search, Header, commentArea
  ])

  .name;

export default componentsModule;
