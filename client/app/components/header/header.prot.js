var constFunctions = require('./../../../protractorConstance');
describe('Header working', function() {
  it('The main page is loaded', () => {
    browser.get('http://localhost:3000');
  });
  it('Date Picker Works', () => {
    var datePicker = element(by.css('.time-container'));
    datePicker.click();
    datePicker.click();
    var input = element(by.css('.ng-pristine'));
    input.click();
    browser.sleep(500);
    var chooseClient = element.all(by.css('.person')),
      number = Math.floor((Math.random() * 6));
    console.log(number);
    browser.sleep(500);
    chooseClient.get(number).click();
    browser.sleep(500);
  });
  it('Forward button work', () => {
    browser.sleep(500);
    var forwardButton = element(by.css('.forward-button'));
    forwardButton.click();
    var pageName = element(by.css('.comment-title'));
    expect(pageName.getText()).toEqual('Leave your comments');
    browser.sleep(500);
  });
  it('Cancel button work', () => {
    browser.sleep(500);
    var cancelButton = element.all(by.css('.cancel-button'));
    cancelButton.get(1).click();
    var pageTittle = element(by.css('.procedure-recent-title'));
    expect(pageTittle.getText()).toEqual('Quick selection');
    browser.sleep(500);
  });

});
