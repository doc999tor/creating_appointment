class HeaderController {
  constructor($rootScope, UserConfiguration, HttpRouter, UrlConfiguration, $timeout) {
    "ngInject";
    this.HttpRouter = HttpRouter;
    this.$timeout = $timeout;
    this.UserConfiguration = UserConfiguration;
    this.$rootScope = $rootScope;
    this.name = 'header';
    this.UrlConfiguration = UrlConfiguration;
    this.curentUser = {
      name: '',
      phone: ''
    }
    this.leftButtonText = this.$rootScope.APP.translates.forwardButtonText;
    this.rightButtonText = this.$rootScope.APP.translates.cancelButtonText;
    this.$rootScope.$watch(() => {
      return $rootScope.APP.notification.status
    }, () => {
      if (this.$rootScope.APP.notification.status) {
        this.$timeout(() => {
          this.$rootScope.APP.notification = {};
        }, this.$rootScope.APP.notificationShowTime);
      }
    });
  }
  callOnLoad() {
    if (this.typePage === "last") {
      this.leftButtonText = this.$rootScope.APP.translates.makeOrderButtonText;
      this.rightButtonText = this.$rootScope.APP.translates.backButtonText;
    }
    this.$rootScope.$on('user:update', () => {
      this.curentUser = this.UserConfiguration.get();
    });
  }
  cancel() {
    if (this.$rootScope.curentSwipeName === 'procedure') {
      this.UrlConfiguration.clearLSProcedure();
      this.$rootScope.$emit('clearProcedureList');
    }
    this.$rootScope.$emit('swiper:changePosition', 'left');

  }
  forward() {
    if (this.typePage === "last") {
      this.$rootScope.APP.notification = {};
      this.HttpRouter.sendDataOfOrder(this.UrlConfiguration.getUrlDataObject()).then(() => {
        this.$rootScope.APP.notification = {
          name: 'isOrder',
          status: true
        }
      });
    } else {
      this.$rootScope.$emit('swiper:changePosition', 'right');
    }
  }
}

export default HeaderController;
