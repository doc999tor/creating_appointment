import template from './commentArea.html';
import controller from './commentArea.controller';
import './commentArea.scss';

let commentAreaComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default commentAreaComponent;
