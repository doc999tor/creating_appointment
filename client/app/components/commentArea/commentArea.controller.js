class CommentAreaController {
  constructor(UrlConfiguration) {
    "ngInject";
    this.UrlConfiguration = UrlConfiguration;
    this.areaText = "";
  }
  pressEnter(keyEvent) {
    if (keyEvent.which === 13)
      this.UrlConfiguration.changeUrl(this.areaText, 'comment', true);
  }
}

export default CommentAreaController;
