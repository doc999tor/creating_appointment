import angular from 'angular';

import commentAreaComponent from './commentArea.component';

let commentAreaModule = angular.module('commentArea', [])

  .component('commentArea', commentAreaComponent)

  .name;

export default commentAreaModule;
