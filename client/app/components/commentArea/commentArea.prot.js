var constFunctions = require('./../../../protractorConstance');

describe("Add comment works", function() {
  it('Last page is loaded', () => {
    constFunctions.loadSwipe();
    var forwardButton = element.all(by.css('.forward-button'));
    forwardButton.get(0).click();
    browser.sleep(1000);
    var textArea = element(by.css('.comment__text'));
    textArea.sendKeys('comment');
    browser.actions()
      .sendKeys(protractor.Key.ENTER)
      .perform();
    browser.sleep(500);
    expect(browser.getCurrentUrl()).toContain("&comment=comment");
  });
});
