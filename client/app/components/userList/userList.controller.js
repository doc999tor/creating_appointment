class UserListController {
  constructor(UserConfiguration, $rootScope, UrlConfiguration) {
    "ngInject";
    this.$rootScope = $rootScope;
    this.UrlConfiguration = UrlConfiguration;
    this.UserConfiguration = UserConfiguration;
    this.isAllUsers = false;
  }
  goToProcedure(id, name, phone) {
    this.UserConfiguration.set(id, name, phone);
    this.UrlConfiguration.clearLSProcedure();

    this.$rootScope.$emit('swiper:changePosition', 'right');
    this.$rootScope.$emit('user:update');
  }
  callOnLoad() {
      if (this.userListTittle === "Simple user"){
          this.isAllUsers = true;
      }
  }
}

export default UserListController;
