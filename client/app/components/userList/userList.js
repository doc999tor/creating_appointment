import angular from 'angular';

import userListComponent from './userList.component';

let userListModule = angular.module('userList', [])

  .component('userList', userListComponent)

  .name;

export default userListModule;
