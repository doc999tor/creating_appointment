import template from './userList.html';
import controller from './userList.controller';
import './userList.scss';

let userListComponent = {
  restrict: 'E',
  bindings: {
    userList: "=",
    userListTittle: "@",
    searcheableName: "="
  },
  template,
  controller
};

export default userListComponent;
