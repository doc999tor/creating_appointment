var constFunctions = require('./../../../protractorConstance');

describe("Select users is working:", function() {
  it('The main page is loaded', () => {
    browser.get('http://localhost:3000/?date=2017-01-01&start=02:00');

  });
  it('Select recent users', () => {
    constFunctions.selectUsersTesting('.recent-users')
  });
  it('Select all users', () => {
    constFunctions.selectUsersTesting('.all-users')
  });
});
