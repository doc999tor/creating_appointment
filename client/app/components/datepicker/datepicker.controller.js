    class DatepickerController {
      constructor(UrlConfiguration, $rootScope, $timeout, $scope, $document, $element) {
        "ngInject";
        this.$element = $element;
        this.$document = $document;
        this.$rootScope = $rootScope;
        this.$scope = $scope;
        this.$timeout = $timeout;
        this.UrlConfiguration = UrlConfiguration;
        this.urlData = {};
        this.timeStart = "";
        this.timeEnd = "";
        this.date = "";
        this.isOpenTimeStart = false;
        this.isOpenTimeEnd = false;
        this.isOpenDate = false;
        this.isDatePicker = false;
        this.isDateTime = false;
        this.isFirstTime = true;
        this.settings = {
          dropdownToggleState: false,
          time: {
            fromHour: '00',
            fromMinute: '00',
            toHour: '00',
            toMinute: '00'
          },
          theme: 'light',
          noRange: false,
          format: 24,
          noValidation: true
        };
      }
      callOnLoad() {
        this.urlData = this.UrlConfiguration.getUrlDataObject();
        this.settings.time.fromHour = this.urlData.start.split(':')[0] || '00';
        this.settings.time.fromMinute = this.urlData.start.split(':')[1] || '00';
        this.settings.time.toHour = this.urlData.end.split(':')[0] || '00';
        this.settings.time.toMinute = this.urlData.end.split(':')[1] || '00';
        this.$rootScope.$on('dataPicker', (event, data) => {
          if (data.prop === 'end') {
            this.settings.time.toHour = data.value.split(':')[0];
            this.settings.time.toMinute = data.value.split(':')[1];
          } else {
            this.settings.time.fromHour = data.value.split(':')[0];
            this.settings.time.fromMinute = data.value.split(':')[1];
          }
        });
      }
      dateOn() {
        this.isOpenDate = true;
        this.isDatePicker = true;
        this.isDateTime = true;
        if (this.isFirstTime) {
          this.$timeout(() => {
            this.$document.on('click', this.callMe.bind(this));
          }, 0);
        }
      }
      callMe() {
        if (this.isFirstTime) {
          this.setDate();
          this.isFirstTime = false;
          this.isOpenDate = false;
          this.isDatePicker = false;
          this.$scope.$apply();
        }
        if (this.isDateTime) {
          this.isDateTime = false;
          return false;
        }
        this.setDate();
        this.isOpenDate = false;
        this.isDatePicker = false;
        this.$scope.$apply();
      };
      onApplyTimePicker(way) {
        let time = new Date("October 13, 2014 " + way.fromHour + ":" + way.fromMinute);
        time = time.toTimeString().split(' ')[0].slice(0, 5);
        this.UrlConfiguration.changeUrl(time, "start", true);
        this.urlData["start"] = time;
        this.isOpenTimeEnd = false;
        this.isOpenTimeStart = false;
        this.$rootScope.$emit('dataPicker', {
          prop: "start",
          value: time
        });
        if (way === 'end') {
          this.UrlConfiguration.changeUrl(true, 'isFixedEndTime', false)
        }
      }
      setDate() {
        let dopArrForTime = this.urlData.date.split('-'),
          i = 2,
          finallyDate = "",
          date = (this.date ? this.date : new Date(dopArrForTime[0], dopArrForTime[1] - 1, dopArrForTime[2]).toLocaleDateString());
        for (i; i >= 0; i--) {
          finallyDate += date.split('.')[i] + "-";
        }
        finallyDate = finallyDate.slice(0, -1);
        this.UrlConfiguration.changeUrl(finallyDate, "date", true);
        this.urlData.date = finallyDate;
        this.isOpenDate = false;
      }
      openTimeBlock(variable) {
        let thisPage = this.$rootScope.curentSwipeName;
        if (thisPage === 'procedure' || thisPage === 'proceduresSummary') {
          return false;
        } else {
          this[variable] = true;
        }
      }
    }
    export default DatepickerController;
