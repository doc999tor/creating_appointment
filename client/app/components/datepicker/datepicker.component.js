import template from './datepicker.html';
import controller from './datepicker.controller';
import './datepicker.scss';

let datepickerComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default datepickerComponent;
