import angular from 'angular';

import datepickerComponent from './datepicker.component';

let datepickerModule = angular.module('datepicker', [])

  .component('datepicker', datepickerComponent)

  .name;

export default datepickerModule;
