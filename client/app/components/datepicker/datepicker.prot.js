var constFunctions = require('./../../../protractorConstance');

fdescribe("Select users is working:", function() {
  it('The main page is loaded', () => {
    browser.get('http://localhost:3000');
  });

  it('Datepicker is working', () => {
    browser.sleep(500);
    var timePicker = element(by.css('.app-time-picker')),
    datePicker = element(by.css('.time-container')),
    confirmField = element(by.css('.info-container')),
    number = Math.floor((Math.random() * 42));;
    timePicker.click();
    var select1 = element(by.model('startingHour')),
      select2 = element(by.model('startingMinute'));
    browser.sleep(500);
    select1.$('[value="02"]').click();
    browser.sleep(500);
    select2.$('[value="22"]').click();
    browser.sleep(500);
    var applyButton = element.all(by.css('.angular-time-picker-push-half--left'));
    applyButton.get(0).click();
    browser.sleep(500);
    datePicker.click();
    var daysPick = element.all(by.css('._day'));
    daysPick.get(number).click();
    browser.sleep(500);
    confirmField.click();
    var checkField = element(by.css('.info-container'));
    browser.sleep(500);
    var checkText = checkField.getText();
    var a = checkText.toString();
    browser.sleep(500);
      expect(browser.getCurrentUrl()).toContain("http://localhost:3000/?start=02:22&date=" + a);
  });
});
