import angular from 'angular';
import HttpRouter from './http-router/http-router';
import UrlConfiguration from './url-configuration/url-configuration';
import UserConfiguration from './user-configuration/user-configuration';

export default angular
  .module('app.services', [])
  .service({
    HttpRouter,
    UrlConfiguration,
    UserConfiguration
  })
.name;
