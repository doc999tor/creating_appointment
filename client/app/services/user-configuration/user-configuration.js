export default class UserConfiguration {
  constructor(UrlConfiguration, $location) {
    "ngInject";
    this.UrlConfiguration = UrlConfiguration;
    this.$location = $location;
    this.userDataParametrs = {
      id: "",
      name: "",
      phone: ""
    };
  }
  set(id, name, phone) {
    this.userDataParametrs.id = id;
    this.userDataParametrs.name = name;
    this.userDataParametrs.phone = phone;
    this.UrlConfiguration.urlDataParameters.client_id = id;
    this.$location.search("client_id", id);
  }
    get(){
        return this.userDataParametrs;
    }
}
