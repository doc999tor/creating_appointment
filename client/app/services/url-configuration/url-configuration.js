export default class UrlConfiguration {
  constructor($location) {
    "ngInject";
    this.$location = $location;
    this.paramsForTransition = "";
    this.urlDataParameters = {
      start: "",
      end: "",
      date: "",
      procedure_time: "",
      procedure_id: [],
      client_id: "",
      comments: ""
    };
    this.arrayOfLSParameters = ['start',
      'end',
      'date',
      'procedure_time',
      'procedure_id',
      'client_id',
      'comments'
    ]
  }
  changeUrl(value, prop, visible) {
    this.urlDataParameters[prop] = value;
    if (visible) {
      this.$location.search(prop, value)
    };
  }
  setUrl(arrayOfParams) {
    let urlData = this.urlDataParameters,
      string = "";
    angular.forEach(arrayOfParams, function(paramsValue) {
      angular.forEach(urlData, function(urlValue, prop) {
        if (prop === paramsValue && urlValue.toString() !== "") {
          if (!angular.isArray(urlValue)) {
            urlValue = [urlValue]
          }
          string += prop + "=" + urlValue.join() + "&";
        }
      });
    });
    this.$location.search(string.slice(0, -1));
  }
  setUrlData(array) {
    this.urlDataParameters = array;
  }
  getUrlDataObject() {
    return this.urlDataParameters;
  }
  addIdForProcedure(id) {
    this.urlDataParameters.procedure_id.push(id);
    this.$location.search('procedure_id', this.urlDataParameters.procedure_id.join());
  }
  deleteProcedure(id) {
    let string;
    for (let i = 0; i < this.urlDataParameters.procedure_id.length; i++) {
      if (this.urlDataParameters.procedure_id[i] === id) {
        this.urlDataParameters.procedure_id.splice(i, 1);
        break;
      }
    }
    string = this.urlDataParameters.procedure_id.length === 0 ? null : this.urlDataParameters.procedure_id.join();
    this.$location.search('procedure_id', string);
  }
  clearLSProcedure() {
    this.urlDataParameters.procedure_id = [];
  }
}
