export default class HttpRouter {

  constructor($http, $q, $rootScope, $httpParamSerializerJQLike) {
    "ngInject";
    this.$http = $http;
    this.$q = $q;
    this.$rootScope = $rootScope;
    this.$httpParamSerializerJQLike = $httpParamSerializerJQLike;
  }

  getBackEndData(typeQuery, searcheableName, countOfElement) {
    let urlServer,
      searchParams = "";
    if (searcheableName) {
      searchParams = "&q=" + searcheableName;
    }
    urlServer = this.$rootScope.APP.serverUrl + "/" + typeQuery + "/?limit=12&offset=" + countOfElement + searchParams;
    let deferred = this.$q.defer();

    this.$http({
      method: "GET",
      url: urlServer
    }).then((data) => {
      deferred.resolve(data.data);
    }, (error) => {
      deferred.reject(error);
    });
    return deferred.promise;
  };

  sendDataOfOrder(data) {
    let deferred = this.$q.defer();

    this.$http({
      method: 'POST',
      url: this.$rootScope.APP.serverUrl + '/appointments/',
      data: this.$httpParamSerializerJQLike({
        date: data.date,
        start: data.start,
        end: data.end ? data.end : data.procedure_time,
        client_id: data.client_id,
        procedure: '[' + data.procedure_id.toString() + ']',
        comments: data.comment
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then((response) => {
      deferred.resolve(response.data);
    }, (response) => {
      deferred.reject(response.data);
    });

    return deferred.promise;
  }
}
